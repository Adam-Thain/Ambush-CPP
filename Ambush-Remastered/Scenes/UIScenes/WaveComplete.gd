extends Control

@onready var text = get_node("P/VB/Label")

func wavecomplete(current_wave):
	visible = true
	text.set_text("Wave Complete")
	await get_tree().create_timer(1.5).timeout
	text.set_text("Wave " + str(current_wave + 1))
	await get_tree().create_timer(1.5).timeout
	visible = false
