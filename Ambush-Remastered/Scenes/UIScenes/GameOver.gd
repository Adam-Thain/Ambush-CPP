extends Control

func gameover():
	get_tree().paused = true
	visible = true

func _on_restart_pressed():
	get_tree().paused = false
	if Engine.get_time_scale() == 2.0:
		Engine.set_time_scale(1.0)
	get_tree().change_scene_to_file("res://Scenes/MainScenes/GameScene.tscn")
	GameData.player_data["Money"] = 1000
	
func _on_exit_game_pressed():
	get_tree().paused = false
	if Engine.get_time_scale() == 2.0:
		Engine.set_time_scale(1.0)
	get_node("/root/SceneHandler").go_to_main_menu("GameScene")
