extends Node

# Called when the node enters the scene tree for the first time.
func _ready():
	load_main_menu()

func load_main_menu():
	get_node("MainMenu/M/VB/NewGame").connect("pressed", Callable(self,"on_new_game_pressed"))
	get_node("MainMenu/M/VB/Settings").connect("pressed", Callable(self,"on_settings_pressed"))
	get_node("MainMenu/M/VB/About").connect("pressed", Callable(self,"on_about_pressed"))
	get_node("MainMenu/M/VB/Quit").connect("pressed", Callable(self,"on_quit_pressed"))
	
func on_new_game_pressed():
	get_node("MainMenu").queue_free()
	var game_scene = load("res://Scenes/MainScenes/GameScene.tscn").instantiate()
	GameData.player_data["Money"] = 1000
	add_child(game_scene)

func on_settings_pressed():
	get_node("MainMenu").queue_free()
	var settings_scene = load("res://Scenes/UIScenes/Settings.tscn").instantiate()
	add_child(settings_scene)

func on_about_pressed():
	get_node("MainMenu").queue_free()
	var about_scene = load("res://Scenes/UIScenes/about.tscn").instantiate()
	add_child(about_scene)

func go_to_main_menu(exiting_scene):
	get_node(exiting_scene).queue_free()
	var main_menu = load("res://Scenes/UIScenes/MainMenu.tscn").instantiate()
	add_child(main_menu)
	load_main_menu()

func on_quit_pressed():
	get_tree().quit()
