extends Control

func resume():
	get_tree().paused = false
	visible = false
	
func pause():
	get_tree().paused = true
	visible = true

func settings():
	return

func exit():
	get_tree().paused = false
	if Engine.get_time_scale() == 2.0:
		Engine.set_time_scale(1.0)
	get_node("/root/SceneHandler").go_to_main_menu("GameScene")

func EscPressed():
	if Input.is_action_just_pressed("esc") and !get_tree().paused:
		pause()
	elif Input.is_action_just_pressed("esc") and get_tree().paused:
		resume()

func _on_resume_pressed():
	resume()

func _on_settings_pressed():
	settings()

func _on_exit_game_pressed():
	exit()

func _process(_delta):
	EscPressed()
